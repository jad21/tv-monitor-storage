# README


sqlite-utils dat_binance_BTCUSDT_1m.db "select count(*) from datafeed where 1 ORDER BY time ASC "
sqlite-utils dat_binance_BTCUSDT_1m.db "select * from datafeed where 1 ORDER BY time ASC limit 1"
sqlite-utils dat_binance_BTCUSDT_1m.db "update datafeed set Time = Time / 1000"

```sh
for file in *df_*; 
do     
    sqlite-utils $file "ALTER TABLE datafeed ADD CloseTime INT;"
done
    # mv -- "$file" "${file//3m/3}"; 
```

```sh
for file in *df_*; 
do     
    echo $file;
    time sqlite-utils $file "update datafeed set CloseTime = 0 where CloseTime is NULL";
done
    # mv -- "$file" "${file//dat/df}"; 
```

```sh
for file in *df_*; 
do     
    echo $file;
    mv -- "$file" "${file//USDT/-USDT}"; 
done
```


```sh
for file in *_1w.db; 
do     
    mv -- "$file" "${file//1w/1W}"; 
done
```